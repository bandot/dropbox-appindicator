# Dropbox AppIndicator

Custom Dropbox appindicator (system tray) and emblem icons to match Suru and Unity desktop panel color scheme

## Screenshot
![](screenshot/dropbox-emblems.png)

## Features
### Emblems
* dropbox app
* dropbox infinite
* dropbox mixedstate
* dropbox selsync
* dropbox syncing
* dropbox unsyncable
* dropbox uptodate

### Status
* dropboxstatus-blank
* dropboxstatus-busy
* dropboxstatus-busy2
* dropboxstatus-idle
* dropboxstatus-logo
* dropboxstatus-x

## Installing
Replace the original icons. 
* Emblems : `~/.dropbox-dist/dropbox-lnx.x86_64-70.4.93/images/emblems`
* Status :  `~/.dropbox-dist/dropbox-lnx.x86_64-70.4.93/images/hicolor/16x16/status`

## License
This project is licensed under the GPLv3 - see the [LICENSE.md](LICENSE.md) file for details